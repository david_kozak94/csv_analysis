# README #

This project is for parsing csv files that are the product of SearchBestie. It is able to create latex tables containing agregated results and also input .dat files for gnuplot.
Project is written in Java, because I needed a training with IO in Java, I had almost never used it before.

### What is this repository for? ###

* Parse csv files from SearchBestie and created latex tables and input files for gnuplot
* version 0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Just clone the repo and fill the __input__ directory with real data

### Who do I talk to? ###

* dkozak94@gmail.com