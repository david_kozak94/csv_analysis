package io.dkozak.csv_analysis.utils;

/**
 * Tri-state boolean
 */
public enum TriBool {
    TRUE,
    FALSE,
    TRALSE;

    public static <T extends Comparable> TriBool compare(T left, T right) {
        int cmp = left.compareTo(right);
        if (cmp > 0)
            return TRUE;
        else if (cmp == 0)
            return TRALSE;
        else
            return FALSE;

    }

    public TriBool not() {
        if (this == TRUE)
            return FALSE;
        else if (this == FALSE)
            return TRUE;
        else
            return TRALSE;

    }
}
