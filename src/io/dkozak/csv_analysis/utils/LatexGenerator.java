package io.dkozak.csv_analysis.utils;

import io.dkozak.csv_analysis.model.Test;
import io.dkozak.csv_analysis.model.Tool;

import java.util.List;
import java.util.Set;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.joining;

/**
 * Contains method for latex generation
 */
public class LatexGenerator {
    public static String createLatexTables(Set<Test> tests) {
        return tests
                .stream()
                .collect(
                        groupingBy(Test::getTestType))
                .entrySet()
                .stream()
                .map(entry -> createLatexTable(entry.getKey().nameForTable(), entry.getValue()))
                .collect(joining("\n"));
    }

    public static String createLatexTable(String caption, List<Test> tests) {
        final String HEADER = "\\begin{table}\n"
                + "\t\\centering\n"
                + "\t\\begin{tabular}";
        final String FOOTER = "\t\t\\hline\n" +
                "\t\\end{tabular}\n"
                + "\t\\caption{" + caption + "}\n"
                + "\t\\label{LABEL}\n"
                + "\\end{table}";

        return HEADER
                + parseTestsIntoLatex(tests)
                + FOOTER;
    }

    private static String parseTestsIntoLatex(List<Test> tests) {
        final String HEADER = "{ | l | l || c | c | c | }\n"
                + "\t\t\\hline\n"
                + "\t\tBenchmark & Tool & Doba běhu [ms] & Odchylka & Počet chyb \\\\\n"
                + "\t\t\\hline\n"
                + "\t\t\\hline\n";
        return HEADER + tests.stream()
                .collect(groupingBy(Test::getBenchmark))
                .entrySet()
                .stream()
                .map(entry -> parseOneLine(entry.getValue()))
                .collect(joining("\t\t\\hline\n"));
    }

    private static String parseOneLine(List<Test> rrCttests) {
        assert rrCttests.size() == 2;

        Test rr, ct, tmp;
        tmp = rrCttests.get(0);
        if (tmp.getTool() == Tool.ROADRUNNER) {
            rr = tmp;
            ct = rrCttests.get(1);
        } else {
            rr = rrCttests.get(1);
            ct = tmp;
        }

        return parseOneLine(rr, ct);
    }

    private static String parseOneLine(Test rr, Test ct) {
        assert rr.getBenchmark() == ct.getBenchmark();
        assert rr.getTool() == Tool.ROADRUNNER;
        assert ct.getTool() == Tool.CONTEST;

        String benchmark = rr.getBenchmark().nameForTable();
        double avgRunRR, avgRunCT, deviationRR, deviationCT;
        int errCountRR, errCountCT;

        avgRunRR = rr.countAvgRun();
        avgRunCT = ct.countAvgRun();
        deviationRR = rr.countDeviation(avgRunRR);
        deviationCT = ct.countDeviation(avgRunCT);
        errCountRR = rr.countErrors();
        errCountCT = ct.countErrors();

        TriBool avgRunRRWon = TriBool.compare(avgRunCT, avgRunRR);
        TriBool deviationRRWon = TriBool.compare(deviationCT, deviationRR);
        TriBool errorsRRWon = TriBool.compare(errCountRR, errCountCT);


        final String DELIM = " & ";
        final String NEWLINE = " \\\\\n";

        String rrline =
                benchmark + DELIM
                        + rr.getTool().nameForTable() + DELIM
                        + conditionalMakeBold((int) avgRunRR, avgRunRRWon) + DELIM
                        + conditionalMakeBold((int) deviationRR, deviationRRWon) + DELIM
                        + conditionalMakeBold(errCountRR, errorsRRWon) + NEWLINE;

        String ctline =
                benchmark + DELIM
                        + ct.getTool().nameForTable() + DELIM
                        + conditionalMakeBold((int) avgRunCT, avgRunRRWon.not()) + DELIM
                        + conditionalMakeBold((int) deviationCT, deviationRRWon.not()) + DELIM
                        + conditionalMakeBold(errCountCT, errorsRRWon.not()) + NEWLINE;

        return "\t\t" + rrline + "\t\t" + ctline;
    }

    private static <T> String conditionalMakeBold(T text, TriBool makeBold) {
        if (makeBold == TriBool.TRUE)
            return "\\textbf{" + text.toString() + "}";
        else return text.toString();
    }

    public interface LatexTableEntity {
        String nameForTable();
    }

}
