package io.dkozak.csv_analysis.utils;

import io.dkozak.csv_analysis.model.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.security.InvalidParameterException;
import java.util.Arrays;
import java.util.Set;

import static java.util.stream.Collectors.toSet;

/**
 * Contains methods for IO operations
 */
public class FileIO {

    public static Set<Test> loadTestsFromDirectory(String directory) {
        File dir = new File(directory);
        if (!dir.isDirectory())
            throw new InvalidParameterException(directory + " is not a directory");
        if (!dir.canRead())
            throw new InvalidParameterException("Cannot read from given directory " + directory);

        if (dir.listFiles() == null)
            throw new InvalidParameterException("Directory " + directory + " is empty");

        return Arrays.stream(dir.listFiles())
                .filter(file -> file.getName().endsWith(".csv"))
                .map(FileHolder::fromFile)
                .map(fileHolder -> Test.createFrom(fileHolder.fileName, fileHolder.fileContent))
                .collect(toSet());
    }

    public static String removeExtension(File file) {

        String name = file.getName();
        return name.substring(0, name.lastIndexOf("."));
    }

    private static class FileHolder {
        public final String fileName;
        public final String[] fileContent;

        public FileHolder(String fileName, String[] fileContent) {
            this.fileName = fileName;
            this.fileContent = fileContent;
        }

        public static FileHolder fromFile(File input) {
            try {
                return new FileHolder(
                        removeExtension(input),
                        Files.lines(input.toPath())
                                .toArray(String[]::new)
                );
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }
    }
}
