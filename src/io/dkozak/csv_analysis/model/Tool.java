package io.dkozak.csv_analysis.model;

import io.dkozak.csv_analysis.utils.LatexGenerator;

/**
 * Used test tools
 */
public enum Tool implements LatexGenerator.LatexTableEntity {
    ROADRUNNER,
    CONTEST;

    @Override
    public String nameForTable() {
        switch (this) {
            case ROADRUNNER:
                return "RoadRunner";
            case CONTEST:
                return "ConTest";
            default:
                return "CANNOT-HAPPEN";
        }
    }
}
