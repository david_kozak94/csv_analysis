package io.dkozak.csv_analysis.model;

import io.dkozak.csv_analysis.utils.LatexGenerator;

/**
 * Represents all known benchmarks
 */
public enum Benchmark implements LatexGenerator.LatexTableEntity {
    AIRLINES,
    ELEVATOR,
    CRAWLER,
    ROVER,
    HEDC,
    CACHE4J;


    @Override
    public String nameForTable() {
        switch (this) {
            case AIRLINES:
                return "Airlines";
            case ELEVATOR:
                return "Elevator";
            case CRAWLER:
                return "Crawler";
            case ROVER:
                return "Rover";
            case HEDC:
                return "HEDC";
            case CACHE4J:
                return "Cache4J";
            default:
                return "CANNOT-HAPPEN";
        }
    }
}
