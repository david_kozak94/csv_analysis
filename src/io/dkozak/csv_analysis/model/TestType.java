package io.dkozak.csv_analysis.model;

import io.dkozak.csv_analysis.utils.LatexGenerator;

import java.security.InvalidParameterException;

/**
 * Used test type
 */
public enum TestType implements LatexGenerator.LatexTableEntity {
    THOUSAND(1000),
    TWENTY(20);

    private final int numOfStates;

    TestType(int numOfStates) {
        this.numOfStates = numOfStates;
    }

    public static TestType createTestTypeFor(int numOfStates) {
        for (TestType testType : values()) {
            if (testType.numOfStates == numOfStates)
                return testType;
        }
        throw new InvalidParameterException("No test type with number of states " + numOfStates);
    }

    public String nameForTable() {
        switch (this) {
            case THOUSAND:
                return "Tisíc náhodně vybraných stavů";
            case TWENTY:
                return "Dvacet zvolených stavů";
            default:
                return "CANNOT-HAPPEN";
        }

    }
}
