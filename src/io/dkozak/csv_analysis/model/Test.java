package io.dkozak.csv_analysis.model;

import jdk.nashorn.internal.ir.annotations.Immutable;

import java.security.InvalidParameterException;
import java.util.Arrays;
import java.util.OptionalDouble;

/**
 * Represents one input CSV file
 */
@Immutable
public class Test {
    private final Tool tool;
    private final Benchmark benchmark;
    private final TestType testType;
    private final String[] colNames;
    private final double[][] data;

    public Test(Tool tool, Benchmark benchmark, TestType testType, String[] data) {
        assert data.length > 1;

        this.tool = tool;
        this.benchmark = benchmark;
        this.testType = testType;
        this.colNames = data[0].split(",");

        this.data = new double[data.length - 1][];
        for (int i = 1; i < data.length; i++) {
            this.data[i - 1] = Arrays.stream(data[i].split(","))
                    .mapToDouble(Double::parseDouble)
                    .toArray();
        }


    }

    public static Test createFrom(String filename, String[] fileContent) {
        String[] parts = filename.split("_");
        if (parts.length != 3)
            throw new RuntimeException("File needs to have a format tool_benchmark_testtype");

        return new Test(
                Tool.valueOf(parts[0].toUpperCase()),
                Benchmark.valueOf(parts[1].toUpperCase()),
                TestType.createTestTypeFor(Integer.valueOf(parts[2])),
                fileContent
        );
    }

    public double countAvgRun() {
        final int index = getTestRunColIndex();
        OptionalDouble average = Arrays.stream(data)
                .mapToDouble(line -> line[index])
                .average();
        if (!average.isPresent())
            throw new RuntimeException("Cannot count average time value for " + this.toString());

        return average.getAsDouble();
    }

    public double countDeviation(final double average) {
        final int index = getTestRunColIndex();
        double sum = Arrays.stream(data)
                .mapToDouble(line -> line[index])
                .map(duration -> Math.abs(duration - average))
                .sum();

        return Math.sqrt(sum / (double) data.length);
    }

    public int countErrors() {
        final int index = getTestRunColIndex() + 1;
        return (int) Arrays.stream(data)
                .mapToDouble(line -> line[index])
                .sum();
    }

    public Tool getTool() {
        return tool;
    }

    public Benchmark getBenchmark() {
        return benchmark;
    }

    public TestType getTestType() {
        return testType;
    }

    public double[][] getData() {
        return data;
    }

    public String[] getColNames() {
        return colNames;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Test test = (Test) o;

        if (tool != test.tool) return false;
        if (benchmark != test.benchmark) return false;
        return testType == test.testType;

    }

    @Override
    public int hashCode() {
        int result = tool.hashCode();
        result = 31 * result + benchmark.hashCode();
        result = 31 * result + testType.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Test{" +
                "tool=" + tool +
                ", benchmark=" + benchmark +
                ", testType=" + testType +
                '}';
    }

    public int getTestRunColIndex() {
        assert this.data.length > 1;
        String colName = null;
        for (int i = 0; i < colNames.length; i++) {
            colName = colNames[i];
            if ("testDuration".equals(colName))
                return i;
        }
        throw new InvalidParameterException("Test run duration not found");

    }
}
