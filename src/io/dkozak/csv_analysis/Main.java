package io.dkozak.csv_analysis;

import io.dkozak.csv_analysis.model.Test;
import io.dkozak.csv_analysis.utils.FileIO;
import io.dkozak.csv_analysis.utils.LatexGenerator;

import java.util.Set;

/**
 * Main Entry of the csv processor, contains configuration
 */
public class Main {
    private static final String INPUT_DIR = "./input/";
    private static final String OUTPUT_DIR = "./output/";

    public static void main(String[] args) {
        Set<Test> tests = FileIO.loadTestsFromDirectory(INPUT_DIR);
        String code = LatexGenerator.createLatexTables(tests);
        System.out.println(code);

    }

}
